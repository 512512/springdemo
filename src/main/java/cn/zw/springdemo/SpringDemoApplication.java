package cn.zw.springdemo;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication(exclude = {RedisRepositoriesAutoConfiguration.class, SolrAutoConfiguration.class})
public class SpringDemoApplication {

	@RequestMapping("hello")
	@ResponseBody
	public String hello(){
		return "Hello Worrld!";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoApplication.class, args);
/*		SpringApplication app=new SpringApplication(SpringDemoApplication.class);
		app.setBannerMode(Banner.Mode.OFF);//关闭banner
		app.run(args);*/
	}
}
